from kivy.app import App
from navigation_screen_manager import NavigationScreenManager
from kivy.properties import ObjectProperty


class MyScreenManager(NavigationScreenManager):
	pass


class TheLabApp(App):
	manager = ObjectProperty()

	def build(self):
		self.manager = MyScreenManager()
		return self.manager


TheLabApp().run()
